function tinhDTB(list, ...datas) {
  let tinhDTB = 0;
  datas.forEach((data) => list.push(data));

  for (var i = 0; i < list.length; i++) {
    tinhDTB += list[i] / list.length;
  }
  return tinhDTB;
}

var DiemTBMonTuNhien = () => {
  var toan = document.getElementById("inpToan").value * 1;
  var ly = document.getElementById("inpLy").value * 1;
  var hoa = document.getElementById("inpHoa").value * 1;
  let tongDtb = tinhDTB([toan], ly, hoa);
  document.getElementById("tbKhoi1").innerHTML = tongDtb.toFixed(2);
};

var diemTBMonXaHoi = () => {
  var van = document.getElementById("inpVan").value * 1;
  var su = document.getElementById("inpSu").value * 1;
  var dia = document.getElementById("inpDia").value * 1;
  var anh = document.getElementById("inpEnglish").value * 1;
  let tongDtb = tinhDTB([van], su, dia, anh);
  document.getElementById("tbKhoi2").innerHTML = tongDtb.toFixed(2);
};
