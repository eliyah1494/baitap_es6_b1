const colorList = [
  "pallet",
  "viridian",
  "pewter",
  "cerulean",
  "vermillion",
  "lavender",
  "celado",
  "saffron",
  "fuschia",
  "cinnabar",
];

var renderButton = () => {
  var contentHTML = "";
  for (var i = 0; i < colorList.length; i++) {
    contentHTML += `<button id = "remove" class = "color-button ${colorList[i]}"></button>`;
  }
  document.getElementById("colorContainer").innerHTML = contentHTML;
};
renderButton();

var colorPicker = document.getElementsByClassName("color-button");
console.log("colorPicker: ", colorPicker);
var result = document.getElementById("house");

var changeColorHouse = () => {
  for (let i = 0; i < colorPicker.length; i++) {
    colorPicker[i].addEventListener("click", function () {
      result.removeAttribute("class");
      result.className = "house " + colorList[i];
    });
  }
};
changeColorHouse();
