let str = document.getElementById("heading").textContent;
let chars = [...str];

var hover = () => {
  var contentHTML = "";
  for (var i = 0; i < chars.length; i++) {
    var span = `<span>${chars[i]}</span>`;
    contentHTML += span;
  }
  var domElement = document.getElementById("heading");
  domElement.innerHTML = contentHTML;
};
hover();
